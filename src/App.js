import React, { Component } from 'react';
import MessagesList from "./containers/MessagesList/MessagesList";

class App extends Component {
  render() {
    return <MessagesList />;
  }
}

export default App;