import React from 'react';
import Moment from 'react-moment';

import './MessageBox.css';

const MessageBox = props => {
  return (
    <div className="MessageBox">
      <div className="PostImage">
        {props.postData.image ?
          <img src={`http://localhost:8000/uploads/${props.postData.image}`} alt="" title={props.postData.author}/>
        : null}
      </div>
      <div className={`PostData ${props.postData.image ? '' : 'Wide'}`}>
        <div className="PostHead">
          <span className="PostAuthor">{props.postData.author || 'Anonymous'}</span>
          <span className="PostDatetime">
            <Moment>{props.postData.datetime}</Moment>
          </span>
        </div>
        <div className="PostMessage">{props.postData.message}</div>
      </div>
    </div>
  )
};

export default MessageBox;