import React, {Component} from 'react';
import './NewMessageForm.css';

class NewMessageForm extends Component {
  state = {
    author: '',
    message: '',
    image: ''
  };

  changeInputHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  confirmFormHandler = event => {
    event.preventDefault();

    if (event.target.name === 'Submit') {
      if (this.state.message === '') return null;

      const formData = new FormData();
      Object.keys(this.state).forEach(key => {
        formData.append(key, this.state[key]);
      });

      this.props.onSubmitForm(formData);
    }

    this.setState({author: '', message: '', image: ''});
  };

  render() {
    return (
      <div className="NewMessageForm">
        <form action="">
          <div className="FormRow">
            <label><span>Author:</span>
              <input type="text" name="author" placeholder="Your name"
                     value={this.state.author}
                     onChange={(e) => this.changeInputHandler(e)}
              />
            </label>
          </div>

          <div className="FormRow">
            <label><span>Message:</span>
              <input type="text" name="message" placeholder="Message" required
                     value={this.state.message}
                     onChange={(e) => this.changeInputHandler(e)}
              />
            </label>
          </div>

          <div className="FormRow">
            <label><span>Image:</span>
              <input type="file" name="image"
                     onChange={(e) => this.fileChangeHandler(e)}
              />
            </label>
          </div>

          <div className="FormRow Buttons">
            <button className="Btn SubmitBtn" name="Submit"
                    onClick={(e) => this.confirmFormHandler(e)}
            >Submit</button>
            <button className="Btn CancelBtn" name="Cancel"
                    onClick={(e) => this.confirmFormHandler(e)}
            >Cancel</button>
          </div>
        </form>
      </div>
    )
  }
}

export default NewMessageForm;