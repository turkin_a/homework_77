import React, {Component} from 'react';
import {connect} from "react-redux";

import './MessagesList.css';
import NewMessageForm from "../../components/NewMessageForm/NewMessageForm";
import MessageBox from "../../components/MessageBox/MessageBox";

import {addNewPost, getPosts} from "../../store/actions";

class MessagesList extends Component {
  componentDidMount() {
    this.props.onGetPosts();
  }

  render() {
    return (
      <div className="Container">
        <NewMessageForm onSubmitForm={this.props.onCreateNewPost} />
        <div className="MessagesList">
          {this.props.postList.map(post => (
            <MessageBox key={post.id} postData={post} />
          ))}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    postList: state.postList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onCreateNewPost: post => dispatch(addNewPost(post)),
    onGetPosts: () => dispatch(getPosts())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesList);