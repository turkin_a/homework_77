import * as actionTypes from './actionTypes';
import axios from '../axios-api';

export const getPostsSuccess = posts => {
  return {type: actionTypes.GET_POSTS_SUCCESS, posts};
};

export const getPostsFailure = error => {
  return {type: actionTypes.GET_POSTS_FAILURE, error};
};

export const addPostSuccess = newPost => {
  return {type: actionTypes.ADD_POST_SUCCESS, newPost};
};

export const addPostFailure = error => {
  return {type: actionTypes.ADD_POST_FAILURE, error};
};

export const getPosts = () => {
  return dispatch => {
    return axios.get('/messages')
      .then(response => dispatch(getPostsSuccess(response.data)))
      .catch(error => dispatch(getPostsFailure(error)));
  }
};

export const addNewPost = post => {
  return dispatch => {
    return axios.post('/messages', post)
      .then(response => dispatch(addPostSuccess(response.data)))
      .catch(error => dispatch(addPostFailure(error)))
  }
};