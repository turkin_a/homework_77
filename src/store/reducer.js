import * as actionTypes from './actionTypes';

const initialState = {
  postList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_POSTS_SUCCESS:
      return {...state, postList: action.posts};
    case actionTypes.ADD_POST_SUCCESS:
      const postList = [...state.postList];
      postList.push(action.newPost);
      return {...state, postList};
    default:
      return state;
  }
};

export default reducer;